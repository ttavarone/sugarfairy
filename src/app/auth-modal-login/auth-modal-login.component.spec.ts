import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthModalLoginComponent } from './auth-modal-login.component';

describe('AuthModalLoginComponent', () => {
  let component: AuthModalLoginComponent;
  let fixture: ComponentFixture<AuthModalLoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthModalLoginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthModalLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
