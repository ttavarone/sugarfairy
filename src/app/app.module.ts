import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { environment } from '../environments/environment';
import { AngularFireModule } from "@angular/fire";
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { AuthService } from "./auth.service";
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthComponent } from './auth/auth.component';
import { AuthModalLoginComponent } from './auth-modal-login/auth-modal-login.component';
import { AuthModalSignupComponent } from './auth-modal-signup/auth-modal-signup.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

//firebase modules we are using in the project
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';

import { CheckboxModule } from 'primeng/checkbox';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

@NgModule({
  declarations: [
    AppComponent,
    AuthComponent,
    AuthModalLoginComponent,
    AuthModalSignupComponent,
  ],
  entryComponents: [
    AuthModalLoginComponent,
    AuthModalSignupComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(environment.firebase), //imports and initializes firebase module
    AngularFireAuthModule,
    AngularFirestoreModule,
    FormsModule,
    NgbModule,
    ReactiveFormsModule,
    FontAwesomeModule
  ],
  providers: [
    AuthService,
    CheckboxModule,
    FontAwesomeModule
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
