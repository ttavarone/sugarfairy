import { Component, OnInit, Input } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import * as $ from "jquery";
import { faShoppingCart } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  title = 'sugar-fairy-ui';
  faShoppingCart = faShoppingCart;

  username: any;
  items: Observable<any[]>;
  constructor(firestore: AngularFirestore){
    this.items = firestore.collection('items').valueChanges();
  }

  ngOnInit(){
	 // PRE LOADER
    window.onload = function(){
      $('.preloader').fadeOut(1000); // set duration in brackets    
    };


  }
}
