import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { UserLogin } from '../user-login-interface';
import { Observable } from 'rxjs';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-auth-modal-signup',
  templateUrl: './auth-modal-signup.component.html',
  styleUrls: ['./auth-modal-signup.component.css']
})
export class AuthModalSignupComponent implements OnInit {

  myForm: FormGroup;

  constructor(public authService: AuthService, public activeModal: NgbActiveModal, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.createForm();
  }

  createForm() {
    this.myForm = this.formBuilder.group({
      email: '',
      password: ''
    });
  }

  submitForm() {
    this.activeModal.close(this.myForm.value);
  }

  googleLogin() {
    // console.log(email, password);
    this.authService.GoogleAuth(); 
  }

  facebookLogin() {
    console.log("Facebook api not setup yet");
    // this.authService.FacebookAuth(); 
  }

}
