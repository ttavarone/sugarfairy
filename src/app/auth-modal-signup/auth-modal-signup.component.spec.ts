import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthModalSignupComponent } from './auth-modal-signup.component';

describe('AuthModalSignupComponent', () => {
  let component: AuthModalSignupComponent;
  let fixture: ComponentFixture<AuthModalSignupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthModalSignupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthModalSignupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
