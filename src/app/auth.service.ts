import { Injectable } from '@angular/core';

import { AngularFireAuth } from "@angular/fire/auth";
import * as firebase from "firebase/app";

import { Observable } from 'rxjs';
import { first } from 'rxjs/internal/operators/first';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  
  user: Observable<firebase.User>;

  constructor(private firebaseAuth: AngularFireAuth) { 
    this.user = firebaseAuth.authState;
  }

  //Sign up through firebase
  signup(email: string, password: string) {
    this.firebaseAuth.createUserWithEmailAndPassword(email, password)
      .then(value => {
        // console.log('Account created successfully', value);
      })
      .catch(err => {
        console.log("butt");
        console.log('Error: ', err.message);
      });
  }

  //Sign in with firebase
  login(email: string, password: string) {
    this.firebaseAuth
      .signInWithEmailAndPassword(email, password)
      .then(value => {
        // console.log(email+' successfully logged in', value);
      })
      .catch(err => {
        console.log('Error: ', err.message);
      });
  }

  //Sign in with Google
  GoogleAuth() {
    return this.AuthLogin(new firebase.auth.GoogleAuthProvider());
  }

  //Sign in with Facebook
  FacebookAuth() {
    return this.AuthLogin(new firebase.auth.FacebookAuthProvider());
  }

  //Auth logic to run auth providers
  AuthLogin(provider) {
    return this.firebaseAuth.signInWithPopup(provider)
      .then((result) => {
        // console.log("Login successful");
      })
      .catch((error) => {
        console.log(error);
      })
  }

  logout() {
    this.firebaseAuth
      .signOut()
      .then((result) => {
        // console.log("User signed out:", result);
      })
      .catch((error) => {
        console.log(error);
      })
  }
}
