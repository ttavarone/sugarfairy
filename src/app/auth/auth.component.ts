import { Component, OnInit, Inject } from '@angular/core';
import { AuthService } from '../auth.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { AuthModalLoginComponent } from '../auth-modal-login/auth-modal-login.component';
import { AuthModalSignupComponent } from '../auth-modal-signup/auth-modal-signup.component';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {
  closeResult = '';
  isUser: boolean = false;
  email: string;
  password: string;


  constructor(public authService: AuthService, private modalService: NgbModal) { }


  ngOnInit(): void {
    this.authService.user
    .subscribe((user) => {
      if(user){
        this.isUser = true;
        console.log(user);
      } else {
        this.isUser = false;
        console.log("User not logged in");
      }
    })
  }

  signup(email,password) {
    // console.log(email, password);
    this.authService.signup(email, password);
  }

  login(email,password) {
    // console.log(email, password);
    this.authService.login(email, password); 
  }

  logout() {
    this.authService.logout();
  }

  openLogin() {
    const modalRef = this.modalService.open(AuthModalLoginComponent);

    modalRef.result
    .then((login) => {
      this.login(login.email,login.password);
    })
    .catch((error) => {
      console.log(error);
    });
  }

  openSignUp() {
    const modalRef = this.modalService.open(AuthModalSignupComponent);

    modalRef.result
    .then((signUp) => {
      this.signup(signUp.email,signUp.password);
    })
    .catch((error) => {
      console.log(error);
    });
  }
}

