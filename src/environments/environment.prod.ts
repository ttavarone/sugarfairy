export const environment = {
  production: true,
  firebase: {
	  apiKey: "AIzaSyCWt9o_BI5B2OuLcLYPcOLXARHupwpXROw",
	  authDomain: "sugar-fairy-prod.firebaseapp.com",
	  databaseURL: "https://sugar-fairy-prod.firebaseio.com",
	  projectId: "sugar-fairy-prod",
	  storageBucket: "sugar-fairy-prod.appspot.com",
	  messagingSenderId: "540853219214",
	  appId: "1:540853219214:web:fc5cb322d891d44384fedf",
	  measurementId: "G-4P35MP5B8S"
	}
};
