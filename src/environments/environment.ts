// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBSF68JqGZLn3voH-smpzlKsAx2-fv5Ek8",
    authDomain: "sugar-fairy-dev-316c5.firebaseapp.com",
    databaseURL: "https://sugar-fairy-dev-316c5.firebaseio.com",
    projectId: "sugar-fairy-dev-316c5",
    storageBucket: "sugar-fairy-dev-316c5.appspot.com",
    messagingSenderId: "817914781490",
    appId: "1:817914781490:web:2e870bca9b2d81017f412b",
    measurementId: "G-KQ2W6T5V90"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
