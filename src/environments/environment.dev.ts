export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBSF68JqGZLn3voH-smpzlKsAx2-fv5Ek8",
    authDomain: "sugar-fairy-dev-316c5.firebaseapp.com",
    databaseURL: "https://sugar-fairy-dev-316c5.firebaseio.com",
    projectId: "sugar-fairy-dev-316c5",
    storageBucket: "sugar-fairy-dev-316c5.appspot.com",
    messagingSenderId: "817914781490",
    appId: "1:817914781490:web:2e870bca9b2d81017f412b",
    measurementId: "G-KQ2W6T5V90"
  }
};
